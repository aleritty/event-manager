Meteor.methods({
  'Loggamelo': function(dest,Cosa){
    var loggedInUser = Meteor.user();if (!loggedInUser) {throw new Meteor.Error(403, "Access denied");}
      var Mitt = Meteor.userId();
      var Quando = moment().toISOString();
      Log.insert({
        mitt:Mitt,
        dest:dest,
        quando:Quando,
        cosa:Cosa
      });
  },
  /******************* FUNZIONI STAFF ******************************/
  'PrimoStaff': function(){
    if(Meteor.isServer && Meteor.users.find().count()<1){
      var id=Accounts.createUser({
          email: 'admin@gestione.cmp',
          password: 'password',
          profile: {
            'nome': 'Admin',
            'cognome': 'Primo',
            'createdBy': 'system',
            'telefono': '000'
          }
      });
      Roles.addUsersToRoles(id,["porta","letti","mensa","registrazione","regstaff","modpers","modstaff","delpers","delstaff","newsint","toteventi"]);
    }
  },
  'createStaff': function(nome,foto,associazione,cognome,email,telefono,password,permessi){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['regstaff'])) {throw new Meteor.Error(403, "Access denied");}
    var profilo = {
      'nome': nome,
      'foto': foto,
      'associazione': associazione,
      'cognome': cognome,
      'createdBy': Meteor.userId(),
      'telefono': telefono
    };
    if(Meteor.isServer){
      var id=Accounts.createUser({
          email: email,
          password: password,
          profile: profilo
      });
      Roles.addUsersToRoles(id,permessi);
      return id;
    }
  },
  'modStaff': function(id,nome,foto,associazione,cognome,email,telefono,permessi,password=false){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['modstaff'])) {throw new Meteor.Error(403, "Access denied");}
    var profilo = {
      'nome': nome,
      'foto': foto,
      'associazione':associazione,
      'cognome': cognome,
      'createdBy': Meteor.userId(),
      'telefono': telefono
    };
    Meteor.users.update(id,{$set:{"profile":profilo,"roles":permessi,"emails.0.address":email}});
    if(password){
      Accounts.setPassword(id, password);
    }
  },
  'delStaff': function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['delstaff'])) {throw new Meteor.Error(403, "Access denied");}
    Meteor.users.remove(dest);
    Meteor.call('Loggamelo',dest,"Eliminato Staff id:"+dest);
  },
  'adminSetPW':function(dest,pw){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['modstaff'])) {throw new Meteor.Error(403, "Access denied");}
    if(Meteor.isServer){
      Accounts.setPassword(dest,pw);
    }
  },
  'cambiacodice': function(oldcode,newcode){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['modstaff'])) {throw new Meteor.Error(403, "Access denied");}
    Persons.update({"codice":oldcode},{$set:{"codice":newcode}});
    Meteor.call('Loggamelo',oldcode,"Modificato il codice di:"+oldcode+" in:"+newcode);
  },
  /******************* FUNZIONI SU PERSONE ******************************/
  'registraPersona': function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['registrazione'])) {throw new Meteor.Error(403, "Access denied");}
    Meteor.call('Loggamelo', dest, "Registrazione ospite");
    Meteor.call('insertIngresso', dest); //ingresso automatico, mettere in un metodo a parte se si fa registrazione ed ingresso separatamente
  },
  'insertIngresso': function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['registrazione','porta'])) {throw new Meteor.Error(403, "Access denied");}
    var Quando=new Date;
    Persons.update({codice:dest},{$set:{status:'presente',dataIngrCamp:Quando,presenza:'interno' }});
    Meteor.call('Loggamelo', dest, "Ingresso al campo ospite");
  },
  'insertPart': function(dest,motivo){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['porta'])) {throw new Meteor.Error(403, "Access denied");}
    var Mitt = Meteor.userId();
    var Quando =new Date();
    Meteor.call('insertMov',dest,"Rientro"); //se il partecipante va via dal campo non può essere fuori al momento dell'addio
    Persons.update({codice:dest},{$set:{status:'partito',motivoPartenzaCamp:motivo,dataPartenzaCamp:Quando }});
    var atlet=Persons.findOne({codice:dest});
    if(!atlet.hasOwnProperty('letti')){
      atlet.letti={"camera":"NON Sistemato","letto":"NON Sistemato"};
    }
    var actsist="Dal:"+atlet.dataIngrCamp+" Al:"+atlet.dataPartenzaCamp+" camera:"+atlet.letti.camera+" Letto:"+atlet.letti.letto;
    Persons.update({codice:dest},{$push:{"history":actsist},$unset:{"letti.letto":"","letti.camera":""}});
  },
  'recuperaPart': function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['porta'])) {throw new Meteor.Error(403, "Access denied");}
    var Mitt = Meteor.userId();
    var Quando =new Date();
    Persons.update({codice:dest},{$set:{status:'presente',dataPartenzaCamp:""}});
    Meteor.call('Loggamelo',dest,"Reinserito tra i presenti al campo");
  },
  'cancellaPers': function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['delpers'])) {throw new Meteor.Error(403, "Access denied");}
    var adesso=new Date();
    var admin=Meteor.user().profile.nome+' '+Meteor.user().profile.cognome+' '+Meteor.userId();
    Meteor.call('Loggamelo',dest, "Cancellato ospite");
    Meteor.call('insertMov',dest,"Rientro");
    Persons.update({codice:dest,status:{ $ne: "cancellato" }},{$set:{
      status:'cancellato',
      motivoPartenzaCamp: 'Cancellato da '+admin,
      dataPartenzaCamp: adesso
    }});
  },
  /******************* FUNZIONI DI BASE ******************************/
  'InsertPasto': function(dest,tipopasto){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['mensa'])) {throw new Meteor.Error(403, "Access denied");}
    var exist=Persons.findOne({"codice":dest,"status":"presente"});
    if(exist){
      var oggi= new Date();
      oggi.setHours(0,0,0);
      var giammangiato=Log.find({"cosa":tipopasto, "quando": { $gt:oggi },"dest":dest}).fetch();
      if(giammangiato.length===0){
        Meteor.call('Loggamelo',dest, tipopasto);
        // var Mitt = Meteor.userId();
        // var Quando = moment().toISOString();
        // Pasti.insert({
        //   mitt:Mitt,
        //   dest:dest,
        //   quando:Quando,
        //   cosa:tipopasto
        // });
      }
    }
  },
  'sbranda': function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['letti'])) {throw new Meteor.Error(403, "Access denied");}
    if(dest){
      Persons.update({codice:dest},{$unset:{"letti":1}});
      Meteor.call('Loggamelo', dest, "Cancellata sistemazione");
    }
  },
  'insertMov': function(cod,cosa){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['porta'])) {throw new Meteor.Error(403, "Access denied");}
    var adesso=new Date();
    var inizio=new Date(2);
    var exist=Persons.findOne({"codice":cod,"status":"presente"});
    if(!exist){
      return false;
    }
    var sefuori=Movimenti.find({"chi":cod,"rientro":{$lt:inizio}}).fetch();
    if(cosa=="Uscita"){
      if(sefuori.length > 0){
        return -1;
      }else{
        Movimenti.insert({
          "uscita": adesso,
          "rientro": new Date(0),
          "chi": cod,
          "staff": Meteor.userId()
        });
        Persons.update({"codice":cod,"status":"presente"}, {$set:{"presenza":"esterno"}});
      }
    }else if(cosa=="Rientro"){
      if(sefuori.length==0){
        return "Mai uscito";
      }else if(sefuori.length>0){
        Movimenti.update({"chi":cod,"rientro":{$lt:inizio}},{$set:{"rientro":adesso}});
        Persons.update({"codice":cod,"status":"presente"}, {$set:{"presenza":"interno"}});
      }
    }else{
      return false;
    }
  },
  'deleteNews':function(dest){
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['newsint'])) {throw new Meteor.Error(403, "Access denied");}
    News.remove({_id:dest});
    Meteor.call('Loggamelo', 'System', "Cancellata news "+dest);
  }

});


//////// Metodi per i campi di testo editabili //////////////////////////////

EditableText.registerCallbacks({
  LettiLogUpdate: function(doc){
    if(typeof doc.letti.letto!=="undefined"){
      var logLetto=" letto: "+doc.letti.letto;
    }else{
      var logLetto="";
    }
    if(typeof doc.letti.camera!=="undefined"){
      var logCamera=" camera: "+doc.letti.camera;
    }else{
      var logCamera="";
    }
    Meteor.call('Loggamelo', doc.codice, "Modificata sistemazione a:"+logLetto+logCamera);
  },
  LettiCheckIfBusy:function(doc,coll,newValue,modifier){
    // if(!doc.hasOwnProperty('letti')){
    //   Persons.update(doc._id,{$set:{'letti':{}}});
    // }
    return newValue;
    // if(modifier.$set.hasOwnProperty('letti.camera')){
    //   if(doc.hasOwnProperty('letti.letto')){
    //     var questoletto=doc.letti.letto;
    //   }else{
    //     var questoletto="";
    //   }
    //   var questacamera=newValue;
    //   var c=Persons.find({"status":"presente","letti.camera":questacamera,"letti.letto":questoletto,"codice":{$not: doc.codice}}).count();
    //   if(c>0){
    //     console.log("errore letto già occupato in questa camera");
    //     throw new Meteor.Error(986, "Sistemazione già occupata");
    //     FlashMessages.sendError('Sistemazione gia occupata');
    //     return false;
    //   }else{
    //     return newValue;
    //   }
    // }else if(modifier.$set.hasOwnProperty('letti.letto')){
    //   var questacamera=doc.letti.camera;
    //   var questoletto=newValue;
    //   var c=Persons.find({"status":"presente","letti.camera":questacamera,"letti.letto":questoletto,"codice":{$not: doc.codice}}).count();
    //   if(c>0){
    //     console.log("errore camera già occupata a questo letto");
    //     throw new Meteor.Error(986, "Sistemazione già occupata");
    //     FlashMessages.sendError('Sistemazione gia occupata');
    //     return false;
    //   }else{
    //     return newValue;
    //   }
    // }
    // return false;
  }
});
