Router.configure({
  layoutTemplate: 'MainTemplate',
  loadingTemplate: 'loading',
  notFoundTemplate: "notFound"
});

// FlowRouter.notFound = {
//   action: function () {
//     BlazeLayout.render("notFound")
//      BlazeLayout.render("MainTemplate", {yield: "notFound"});
//   }
// }

Router.route('/', function () {
  if (!Meteor.userId()) {
    this.render('fullPageAtForm');
  }else{
    this.render('MainContent');
  }
});
// FlowRouter.route('/', {
//     action: function() {
//       if (!Meteor.userId()) {
//         BlazeLayout.render("MainTemplate", {yield: "fullPageAtForm"});
//       }else{
//         BlazeLayout.render("MainTemplate", {yield: "MainContent"});
//       }
//     }
// });


Router.route('/admin-news',function(){
  this.render("AdminNews");
});
// FlowRouter.route('/admin-news', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "AdminNews"});
//     },
// subscriptions: function() {
//  this.register('myNews', Meteor.subscribe('FeedNews'));
// },
// });
// //check like this: FlowRouter.subsReady("myNews");
// //check all subscriptions like this FlowRouter.subsReady();


Router.route('/registrazione',function(){
  this.render("registrazione");
});
// FlowRouter.route('/registrazione', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "registrazione"});
//     }
// });

Router.route('/accettazione',function(){
  this.render("AdminRegistrati");
}); // DA INSERIRE SE SI VOGLIONO FARE LE REGISTRAZIONI ONLINE E POI ACCETTAZIONE AL BANCO DI INGRESSO
// FlowRouter.route('/accettazione', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "AdminRegistrati"});
//     }
// });

Router.route('/cambiacodice',function(){
  this.render("cambiacodice");
});
// FlowRouter.route('/cambiacodice', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "cambiacodice"});
//     }
// });

Router.route('/admin-staff',function(){
  this.render("AdminStaff");
});
// FlowRouter.route('/admin-staff', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "AdminStaff"});
//     }
// });

Router.route('/admin-staff/:uId',function (){
  var loggedInUser=Meteor.user();if(!loggedInUser||!Roles.userIsInRole(loggedInUser,['modstaff','delstaff'])){this.render("roleError");}else{
    this.render('AdminStaff',{
      data: function(){
        return {codice:this.params.uId};
      }
    });
  }
});
// FlowRouter.route('/admin-staff/:uId', {
//     action: function(params) {
//       BlazeLayout.render("MainTemplate", {yield: "AdminStaff",codice:params.uId});
//     }
// });

Router.route('/Postiletto',function(){
  this.render("Postiletto");
});
// FlowRouter.route('/Postiletto', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "Postiletto"});
//     }
// });

Router.route('/mensa',function (){
  this.render('Mensa');
});
// FlowRouter.route('/mensa', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "Mensa"});
//     }
// });

Router.route('/libro-eventi');
// FlowRouter.route('/Libro-eventi', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "libro-eventi"});
//     }
// });

Router.route('/movimenti',function(){
  this.render("Movimenti");
});
// FlowRouter.route('/movimenti', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "Movimenti"});
//     }
// });

Router.route('/partenze',function(){
  this.render("Partenze");
});
// FlowRouter.route('/partenze', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "Partenze"});
//     }
// });

Router.route('/admin-iscritti',function(){
  this.render("AdminIscritti");
});
// FlowRouter.route('/admin-iscritti', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "AdminIscritti"});
//     }
// });

Router.route('/profilopers/:uId',function (){
  this.render('profilopers',{
    data: function(){
      return {codice:this.params.uId};
    }
  });
});
// FlowRouter.route('/profilopers/:uId', {
//     action: function(params) {
//       BlazeLayout.render("MainTemplate", {yield: "profilopers",codice:params.uId});
//     }
// });

Router.route('/admin-db',function(){
  this.render("admindb",{
    data: {loggedInUser}
  });
});
// FlowRouter.route('/admin-db', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "admindb", data:loggedInUser});
//     }
// });


Router.route('/report');
// FlowRouter.route('/report', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "report"});
//     }
// });

Router.route('/report/presenti',function (){
  this.render('reportpresenti');
});
// FlowRouter.route('/report/presenti', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "reportopresenti"});
//     }
// });

Router.route('/report/pasti',function (){
  this.render('reportpasti');
});
// FlowRouter.route('/report/pasti', {
//     action: function() {
//       BlazeLayout.render("MainTemplate", {yield: "reportpasti"});
//     }
// });
