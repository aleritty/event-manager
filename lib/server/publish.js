Meteor.publish('FeedNews', function() {
  return News.find();
});

Meteor.publish('TotalPersons', function() {
  return Persons.find();
});
Meteor.publish('OnePerson',function(userId){
  return Persons.find({codice:userId});
});
Meteor.publish('PresentPersons', function() {
  return Persons.find({"presenza":"interno"});
});
Meteor.publish('AbsentPersons', function() {
  return Persons.find({"presenza":"esterno"});
});
Meteor.publish('PersoneRegistrate', function() {
  return Persons.find({"status":"registrato"});
});
Meteor.publish('PersonePresenti', function() {
  return Persons.find({"status":"presente"});
});
Meteor.publish('PersonePartite', function() {
  return Persons.find({"status":"partito"});
});
Meteor.publish('PersoneCancellate', function() {
  return Persons.find({"status":"cancellato"});
});
Meteor.publish('PersoneAgibili', function() {
  return Persons.find({"status":"presente","presenza":"interno"});
});

Meteor.publish('FeedStaff', function() {
  if (Roles.userIsInRole(this.userId, ['regstaff','modstaff','delstaff'])) {
    return Meteor.users.find();
  }else{
    return Meteor.users.find({_id:this.userId});
  }
});
Meteor.publish('LogOggi', function() {
  var oggi= new Date();oggi.setHours(0,0,0);
  if (Roles.userIsInRole(this.userId, ['toteventi'])) {
    return Log.find({"quando":{"$gte": oggi}});
  }else{
    return Log.find({mitt:this.userId,"quando":{"$gte": oggi}});
  }
});
Meteor.publish('LibroLog', function(iniz=new Date(0),fine=new Date()) {
  if (Roles.userIsInRole(this.userId, ['toteventi'])) {
    return Log.find({"quando":{"$gte": iniz,"$lt":fine}});
  }else{
    return Log.find({mitt:this.userId,"quando":{"$gte": iniz,"$lt":fine}});
  }
});
Meteor.publish('LogPasti', function() {
  var oggi= new Date();oggi.setHours(0,0,1);
  return Log.find({"cosa":{$in:['Colazione','Pranzo','Cena']},"quando":{"$gte": oggi}});
});

Meteor.publish('LogReception', function(iniz=new Date(0),fine=new Date()) {
  if (Roles.userIsInRole(this.userId, ['toteventi'])) {
    return Log.find({"cosa":{$in:['Ingresso al campo ospite','Registrazione ospite']},"quando":{"$gte": iniz,"$lt":fine}});
  }else{
    return Log.find({"cosa":{$in:['Ingresso al campo ospite','Registrazione ospite']},mitt:this.userId,"quando":{"$gte": iniz,"$lt":fine}});
  }
});


Meteor.publish('FeedMovimenti',function(){
  if (Roles.userIsInRole(this.userId, ['porta'])) {
    return Movimenti.find();
  }else{
    return [];
  }
});
