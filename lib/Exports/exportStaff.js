Router.route('/exportStaff', function() {
  var data = Meteor.users.find().fetch();
  var fields = [
    {
      key: 'profile.nome',
      title: 'Nome',
      type: 'string',
    },
    {
      key: 'profile.cognome',
      title: 'Cognome',
      type: 'string',
    },
    {
      key: 'profile.telefono',
      title: 'Telefono',
      type: 'string',
    },
    {
      key: 'emails.0.address',
      title: 'Email',
      type: 'string',
    },
    {
      key: 'profile.createdBy',
      title: 'Creato da',
      type: 'String',
    }
  ];
  var ora=moment().format("YYYY-MM-DD_HH-mm");
  var title = 'Staff-CAT-Ventimiglia-al-'+ora;
  var file = Excel.export(title, fields, data);
  var headers = {
    'Content-type': 'application/vnd.openxmlformats',
    'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
  };
  this.response.writeHead(200, headers);
  this.response.end(file, 'binary');
}, { where: 'server' });
