Router.route('/exportIscritti', function() {
  var data = Persons.find({status:"presente"}).fetch();
  var fields = [
    {
      key: 'Nome',
      title: 'Nome',
      type: 'string',
    },
    {
      key: 'Cognome',
      title: 'Cognome',
      type: 'string',
    },
    {
      key: 'codice',
      title: 'Codice Ospite',
      type: 'string'
    },
    {
      key: 'tespol',
      title: 'N° Tessera',
      type: 'string',
    },
    {
      key: 'codfisc',
      title: 'Codice Fiscale',
      type: 'string',
    },
    {
      key: 'Telefono',
      title: 'Telefono',
      type: 'string',
    },
    {
      key: 'Nazione',
      title: 'Nazione',
      type: 'string',
    },
    {
      key: 'birthday',
      title: 'Data di Nascita',
      type: 'Date',
      transform: function(val){
        return moment(val).format("YYYY/MM/DD");
      }
    },
    {
      key: 'luognasc',
      title: 'Luogo di Nascita',
      type: 'string',
    },
    {
      key: 'sesso',
      title: 'Sesso',
      type: 'string',
    },
    {
      key: 'ingrdate',
      title: 'Data di ingresso in Italia',
      type: 'Date',
      transform: function(val){
        if(val !== null && val !== ""){
          return moment(val).format("YYYY/MM/DD");
        }else{
          return "";
        }
      }
    },
    {
      key: 'vismedsel',
      title: 'Visita Medica',
      type: 'string',
    },
    {
      key: 'vismeddate',
      title: 'Data visita',
      type: 'string',
      transform: function(val){
        if(val !== null && val !== "1970-01-01T00:00:00.000Z" && val !== ""){
          return moment(val).format("YYYY/MM/DD");
        }else{
          return "";
        }
      }
    },
    {
      key: 'permsogsel',
      title: 'Permesso di soggiorno',
      type: 'string',
    },
    {
      key: 'permsogdate',
      title: 'Scadenza permesso',
      type: 'Date',
      transform: function(val){
        if(val !== null && val !== "1970-01-01T00:00:00.000Z" && val !== ""){
          return moment(val).format("YYYY/MM/DD");
        }else{
          return "";
        }
      }
    },
    {
      key: 'richassel',
      title: 'Richiesta asilo politico',
      type: 'string',
    },
    {
      key: 'richasdate',
      title: 'Data richiesta',
      type: 'Date',
      transform: function(val){
        if(val !== null && val !== "1970-01-01T00:00:00.000Z" && val !== ""){
          return moment(val).format("YYYY/MM/DD");
        }else{
          return "";
        }
      }
    },
    {
      key: 'dataIngrCamp',
      title: 'Data di ingresso al campo',
      type: 'Date',
      transform: function(val){
        return moment(val).format("YYYY/MM/DD");
      }
    },
    {
      key: 'note',
      title: 'Note',
      type: 'string',
    }
  ];
  var ora=moment().format("YYYY-MM-DD_HH-mm");
  var title = 'Ospiti-CAT-Ventimiglia-al-'+ora;
  var file = Excel.export(title, fields, data);
  var headers = {
    'Content-type': 'application/vnd.openxmlformats',
    'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
  };
  this.response.writeHead(200, headers);
  this.response.end(file, 'binary');
}, { where: 'server' });
