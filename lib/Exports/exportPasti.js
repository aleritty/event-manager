Router.route('/exportPasti', function() {
  var colazioni=Log.find({"cosa":'Colazione'},{sort:{quando:1}}).fetch();
  var pranzi=Log.find({"cosa":'Pranzo'},{sort:{quando:1}}).fetch();
  var cene=Log.find({"cosa":'Cena'},{sort:{quando:1}}).fetch();

  var dictColazioni = _.countBy(colazioni,function(pasto){
    var appr=pasto.quando;appr.setHours(0,0,0);
    return appr
  });
  var dictPranzi = _.countBy(pranzi,function(pasto){
    var appr=pasto.quando;appr.setHours(0,0,0);
    return appr
  });
  var dictCene = _.countBy(cene,function(pasto){
    var appr=pasto.quando;appr.setHours(0,0,0);
    return appr
  });

  var data=[];
  Object.keys(dictCene).forEach(function(key){
    var obj={"Giorno":key,"Colazioni":dictColazioni[key],"Pranzi":dictPranzi[key],"Cene":dictCene[key]};
    var tempo=new Date(obj.Giorno);
    var newgiorno=moment(tempo).format("YYYY/MM/DD");
    obj.Giorno=newgiorno;
    data.push(obj);
  })
  var fields = [
    {
      key: 'Giorno',
      title: 'Giorno',
      type: 'Date',
    },
    {
      key: 'Colazioni',
      title: 'N. Colazioni',
      type: 'Number',
    },
    {
      key: 'Pranzi',
      title: 'N. Pranzi',
      type: 'Number',
    },
    {
      key: 'Cene',
      title: 'N. Cene',
      type: 'Number',
    }
  ];


  var ora=moment().format("YYYY-MM-DD_HH-mm");
  var title = 'Pasti-CAT-Ventimiglia-al-'+ora;
  var file = Excel.export(title, fields, data);
  var headers = {
    'Content-type': 'application/vnd.openxmlformats',
    'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
  };
  this.response.writeHead(200, headers);
  this.response.end(file, 'binary');
}, { where: 'server' });
