Router.route('/exportMovimenti', function() {
  var data = Movimenti.find({}, {sort: {'rientro': -1}}).fetch();
  var fields = [
    {
      key: 'chi',
      title: 'Ospite',
      type: 'string',
      transform: function(val) {
        var pers=Persons.findOne({codice:val});
        return pers.Nome+" "+pers.Cognome;
      }
    },
    {
      key: 'chi',
      title: 'Codice Ospite',
      type: 'string'
    },
    {
      key: 'uscita',
      title: 'Data Uscita',
      type: 'Date',
      transform: function(val) {
        return moment(val).format("YYYY/MM/DD HH:mm");
      }
    },
    {
      key: 'rientro',
      title: 'Data Rientro',
      transform: function(val) {
        if(moment(val).isBefore(2)){
          return "FUORI CAMPO";
        }else{
          return moment(val).format("YYYY/MM/DD HH:mm");
        }
      }
    }
  ];
  var ora=moment().format("YYYY-MM-DD_HH-mm");
  var title = 'Movimenti-CAT-Ventimiglia-al-'+ora;
  var file = Excel.export(title, fields, data);
  var headers = {
    'Content-type': 'application/vnd.openxmlformats',
    'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
  };
  this.response.writeHead(200, headers);
  this.response.end(file, 'binary');
}, { where: 'server' });
