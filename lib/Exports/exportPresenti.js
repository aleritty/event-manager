Router.route('/exportPresenti', function() {
  var report=[];
  var ricerca=[];
  var inizio=new Date(2);
  var persFuori=Movimenti.find({"rientro":{$lt:inizio}}).fetch();
  persFuori.forEach(function(uno){
    var perFu=Persons.findOne({"status":"presente","codice":uno.chi},{fields:{'Nome':1,"Cognome":1,"codice":1}});
    if(typeof perFu !== "undefined"){
      ricerca.push(uno.chi);
      perFu['stato']='ASSENTE';
      perFu['datamov']=moment(uno.uscita).format("YYYY/MM/DD HH:mm");;
      report.push(perFu);
    }
  });
  var persDentro=Persons.find({"status":"presente",codice:{$nin:ricerca}},{fields:{'Nome':1,"Cognome":1,"codice":1}}).fetch();
  persDentro.forEach(function(uno){
    uno['stato']='PRESENTE';
    uno['datamov']="";
    report.push(uno);
  });
  var fields = [
    {
      key: 'Nome',
      title: 'Nome',
      type: 'string',
    },
    {
      key: 'Cognome',
      title: 'Cognome',
      type: 'string',
    },
    {
      key: 'codice',
      title: 'Codice Ospite',
      type: 'string'
    },
    {
      key: 'stato',
      title: 'STATO',
      type: 'string'
    },
    {
      key: 'datamov',
      title: 'Data Ultima uscita (se assente)',
      type: 'Date',
      transform: function(val){
        if(val !== null && val !== "1970-01-01T00:00:00.000Z" && val !== ""){
          return moment(val).format("YYYY/MM/DD");
        }else{
          return "";
        }
      }
    }
  ];
  var ora=moment().format("YYYY-MM-DD_HH-mm");
  var title = 'Presenti-CAT-Ventimiglia-al-'+ora;
  var file = Excel.export(title, fields, report);
  var headers = {
    'Content-type': 'application/vnd.openxmlformats',
    'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
  };
  this.response.writeHead(200, headers);
  this.response.end(file, 'binary');
}, { where: 'server' });
