if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("ElencoFuoricampo", TabularTables);
TabularTables.ElencoFuoricampo = new Tabular.Table({
  name: "ElencoFuoricampo",
  collection: Movimenti,
  limit: 500,
  selector: function(){
    var inizio=new Date(2);
    return {"rientro":{$lt:inizio}};
  },
  order: [[2, "desc"]],
  columns: [
    { data: 'chi', title: 'Nome', render:function(value){
      var pers=Persons.findOne({"codice":value});
      if(pers){
        return pers.Nome+" "+pers.Cognome;
      }
    }},
    { data: 'chi', title: 'Codice'},
    { data: 'uscita', title: 'Uscita', render:function(value){return moment(value).format('DD/MM/YYYY HH:mm');}}
  ]
});

Meteor.isClient && Template.registerHelper("MovimentiTotali", TabularTables);
TabularTables.MovimentiTotali = new Tabular.Table({
  name: "MovimentiTotali",
  collection: Movimenti,
  limit: 500,
  order: [[2, "desc"]],
  columns: [
    { data: 'chi', title: 'Nome',render:function(value){
      var pers=Persons.findOne({"codice": value});
      if(pers){
        return pers.Nome+" "+pers.Cognome;
      }
    }},
    { data: 'chi', title: 'Codice' },
    { data: 'uscita', title: 'Uscita',render:function(value){return moment(value).format('DD/MM/YYYY HH:mm');}},
    { data: 'rientro', title: 'Rientro',render:function(value,type,obj){
      var inizio=new Date(0).getTime();
      if(value.getTime()===inizio){
        if(moment().diff(moment(obj.uscita),'days')>3){
          var testo='<span style="background-color:red;">da oltre 3 giorni</span>';
        }else{var testo='';}
        return new Spacebars.SafeString('<span class="bg-info">&nbsp;FUORI&nbsp;'+testo+'</span>');
      }else{
        return moment(value).format('DD/MM/YYYY HH:mm');
      }
    }},
    { data: 'staff', title: 'Staff',render:function(value){
      var pers=Meteor.users.findOne({_id:value});
      if(pers){
        return pers.profile.nome+" "+pers.profile.cognome;}},
      }
  ]
});
