if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("LogProfiloPers", TabularTables);
TabularTables.LogProfiloPers = new Tabular.Table({
  name: "LogProfiloPers",
  collection: Log,
  limit: 500,
  order: [[0, "desc"]],
  columns: [
    { data: 'quando', title: 'Quando',sortOrder: 0, sortDirection: 'descending',render:function(value){return moment(value).format('YYYY/MM/DD HH:mm');}},
    { data: 'mitt', title: 'Staff',render:function(value){
      var pers=Meteor.users.findOne({_id:value});
      if(pers){
        return pers.profile.nome+" "+pers.profile.cognome;
      }
    }},
    { data: 'dest', title: 'Codice Ospite' },
    { data: 'cosa', title: 'Azione'}
  ]
});
