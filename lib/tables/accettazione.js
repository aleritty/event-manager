if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("ElencoRegistrati", TabularTables);
TabularTables.ElencoRegistrati = new Tabular.Table({
  name: "ElencoRegistrati",
  collection: Persons,
  selector: function(){
    return {"status": "registrato"}
  },
  limit: 500,
  order: [[1, "desc"],[0,"desc"]],
  columns: [
    { data: 'Nome',label:'Nome'},
    { data: 'Cognome',label:'Cognome'},
    { data: 'tespol', label: 'N tessera'},
    { data: 'codice',label:'Codice'},
    { data: 'codice', title: 'Azioni',
      render: function (value) {
        return new Spacebars.SafeString('<button class="btn btn-primary recupera" id="REC'+value+'">Recupera</button>');
      }
    }
  ]
});
