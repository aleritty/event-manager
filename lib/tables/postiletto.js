if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("PostiLettoAssegnati", TabularTables);
TabularTables.PostiLettoAssegnati = new Tabular.Table({
  name: "PostiLettoAssegnati",
  collection: Persons,
  limit: 500,
  selector: function(){
    return {"letti.camera":{$exists:true},"letti.letto":{$exists:true},"status":"presente"}
  },
  order: [[3, "desc"],[4,"desc"]],
  columns: [
    { data: 'Nome', title: 'Nome' },
    { data: 'Cognome', title: 'Cognome' },
    { data: 'codice', title: 'Codice'},
    { data: 'letti.camera', title: 'Camera', cellClass: 'editable-text-trigger', tmpl: Meteor.isClient && Template.cellaCamera },
    { data: 'letti.letto', title: 'Letto', cellClass: 'editable-text-trigger', tmpl: Meteor.isClient && Template.cellaLetto },
    { data: 'codice', title: 'X', render: function (val) {
      return new Spacebars.SafeString("<button id=\""+val+"\" class=\"btn btn-sm btn-danger cancellabtn\"><span class=\"glyphicon glyphicon-remove\"></span></button>") } }
  ]
});

Meteor.isClient && Template.registerHelper("PostiLettoVacanti", TabularTables);
TabularTables.PostiLettoVacanti = new Tabular.Table({
  name: "PostiLettoVacanti",
  collection: Persons,
  limit: 500,
  selector: function(){
    return {$or:[{"letti.camera":{$exists:false}},{"letti.letto":{$exists:false}}],"status":"presente"}
  },
  order: [[3, "desc"],[4,"desc"]],
  columns: [
    { data: 'Nome', title: 'Nome' },
    { data: 'Cognome', title: 'Cognome' },
    { data: 'codice', title: 'Codice'},
    { data: 'letti.camera', title: 'Camera', cellClass: 'editable-text-trigger', tmpl: Meteor.isClient && Template.cellaCamera },
    { data: 'letti.letto', title: 'Letto', cellClass: 'editable-text-trigger', tmpl: Meteor.isClient && Template.cellaLetto }
  ]
});
