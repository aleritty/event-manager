if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("ElencoPresenti", TabularTables);
TabularTables.ElencoPresenti = new Tabular.Table({
  name: "ElencoPresenti",
  collection: Persons,
  limit: 500,
  selector: function() {
    return { "presenza":"interno"}
  },
  order: [[1, "desc"],[0,"desc"]],
  columns: [
    {data: "Nome", title: "Nome"},
    {data: "Cognome", title: "Cognome"},
    {data: "codice", title: "Codice"}
  ]
});

Meteor.isClient && Template.registerHelper("ElencoAssenti", TabularTables);
TabularTables.ElencoAssenti = new Tabular.Table({
  name: "ElencoAssenti",
  collection: Persons,
  limit: 500,
  selector: function() {
    return { "presenza":"esterno"}
  },
  order: [[1, "desc"],[0,"desc"]],
  columns: [
    {data: "Nome", title: "Nome"},
    {data: "Cognome", title: "Cognome"},
    {data: "codice", title: "Codice"}
  ]
});
