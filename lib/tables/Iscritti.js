if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("ElencoIscritti", TabularTables);
TabularTables.ElencoIscritti = new Tabular.Table({
  name: "ElencoIscritti",
  collection: Persons,
  limit: 500,
  selector:function(){
    return {"status": "presente"};
  },
  order: [[2, "desc"],[1,"desc"]],
  columns: [
    { data: 'foto',title:'Foto', render:function(val){
      if(val==undefined){
        val="/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAgACAAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+uKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiipLW2mvJ44II2lmkO1UQZJNAEdFep+GvhTBAiz6ufOlxkW8bYRfqRyT9OPrXd2WkWOmqFtrSGAD/nnGFoA+caK+kLzTLTUVIubaG5U9pYw3864jxH8KbS8RptKb7HcdfKckxt/Ufy9qAPJqKnvrGfTbuS3uY2imQ4ZD2qCgAooooAKKKKACiiigAooooAK9m+Hvg9dC09by4QHUJxk5HMSnoo9/X8u1ebeBtKXV/E9nC4zGjea49l5x+JwPxr36gAooooAKKKKAOT8c+Eo/EmnNJGgGoQqTG46sP7h9j29D+NeIMpRirAqwOCD1FfTdeG/ErSV0zxTOUXbHcqJwB6nIb9QT+NAHLUUUUAFFFFABRRRQAUUUUAdx8IFB8T3GQMi0bH/faV7HXhXw41Ead4utN/3J90JPuRx+oFe60AFFFFABRRRQAV5N8ZAP7V08458luf8AgVes14n8VNQF54qaJDlbaJYjj15Y/wDoWPwoA5CiiigAooooAKKKKACiiigBY5GhdXRirqQVYdQfWve/CPiOPxLpEU+5ftCALOg/hf8AwPUf/WrwOtXw1rd/ompJLYBpZH+VoACwlHoQKAPoais7SbyXULKO4mtJbJ2+9DNjcP8A635fStGgAooqlqV49hZyTpbS3TKOIYQCzUAVPEmuQeHNKmvJiCVGI488u/Yf57ZrwC6uZL25luJm3yyuXdvUk5Na3i3xBqGvaizXyNb+XkLbEEBB9D39TWLQAUUUUAFFFFABRRRQAUUV1ngDwYfEt4bi5BGnwMN//TRv7o/r/wDXoAZ4Q8BXfiZhcSE2ung8ykcv7KP69PrXrei+HdP0CAJZWwibGGkIy7fU/wCRWjDElvEI4lCRqAqqowAB2FTUAFFFFABRRRQBk614e0/X7cxXtskuB8r9HX6N1FeSeMPAF14bLXEJN1p+f9bj5o/Zh/X+Ve41DNElxEY5VDxsCrKwyCD2NAHzVRXXeP8AwX/wjl0Lq1UnTpmwB18pv7p9vT/OeRoAKKKKACiiigC1pWnTaxqNvZQDMszhRnoPUn2Ayfwr6B0fS4NG06CytxtiiXaD3J7k+5PNedfCDRhJcXepuuRH+4iPueW/TH5mvVaACiiigAooooAKKKKACiiigChq2mw6vYT2c4DQzKVPqPQj3B5r5+1fS5dG1O5s5v8AWQuVzj7w7H8Rg19IV5b8YNGAaz1RF+9+4lI/NT/6F+lAHm1FFFABRRRQB7r8PLAWHhKw4w0qmZj67jkfpiumqlpFuLbSrKHp5cKJ+SgVdoAKKKKACiiigAooooAKKKKACub8e2H9peE9QTHzRxmZT6FfmP6Aj8a6SquoxC5sbqI9HiZfzBFAHzdRRRQB/9k=";
      }
      return new Spacebars.SafeString('<img src="data:image/jpeg;base64,'+val+'">')
    }},
    { data: 'Nome',title:'Nome'},
    { data: 'Cognome',title:'Cognome'},
    { data: 'tespol', title: 'N tessera'},
    { data: 'Nazione', title: 'Nazionalità'},
    { data: 'codice',title:'Codice'},
    { data: 'codice', title: 'Azioni',
      render: function (value) {
        return new Spacebars.SafeString('<a href="/profilopers/'+value+'" class="btn btn-primary">Profilo</a>&nbsp;<a href="/badgepdf/'+value+'" class="btn btn-primary" target="_blank">BADGE</a>')
      }
    }
  ]
});

Meteor.isClient && Template.registerHelper("ElencoIscrittiMinori", TabularTables);
TabularTables.ElencoIscrittiMinori = new Tabular.Table({
  name: "ElencoIscrittiMinori",
  collection: Persons,
  limit: 500,
  selector:function(){
    var maytoday=moment().subtract(18, 'years')._d;
    return {"status": "presente","birthday":{$gt:maytoday}};
  },
  order: [[2, "desc"],[1,"desc"]],
  columns: [
    { data: 'foto',title:'Foto', render:function(val){
      if(val==undefined){
        val="/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAgACAAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+uKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiipLW2mvJ44II2lmkO1UQZJNAEdFep+GvhTBAiz6ufOlxkW8bYRfqRyT9OPrXd2WkWOmqFtrSGAD/nnGFoA+caK+kLzTLTUVIubaG5U9pYw3864jxH8KbS8RptKb7HcdfKckxt/Ufy9qAPJqKnvrGfTbuS3uY2imQ4ZD2qCgAooooAKKKKACiiigAooooAK9m+Hvg9dC09by4QHUJxk5HMSnoo9/X8u1ebeBtKXV/E9nC4zGjea49l5x+JwPxr36gAooooAKKKKAOT8c+Eo/EmnNJGgGoQqTG46sP7h9j29D+NeIMpRirAqwOCD1FfTdeG/ErSV0zxTOUXbHcqJwB6nIb9QT+NAHLUUUUAFFFFABRRRQAUUUUAdx8IFB8T3GQMi0bH/faV7HXhXw41Ead4utN/3J90JPuRx+oFe60AFFFFABRRRQAV5N8ZAP7V08458luf8AgVes14n8VNQF54qaJDlbaJYjj15Y/wDoWPwoA5CiiigAooooAKKKKACiiigBY5GhdXRirqQVYdQfWve/CPiOPxLpEU+5ftCALOg/hf8AwPUf/WrwOtXw1rd/ompJLYBpZH+VoACwlHoQKAPoais7SbyXULKO4mtJbJ2+9DNjcP8A635fStGgAooqlqV49hZyTpbS3TKOIYQCzUAVPEmuQeHNKmvJiCVGI488u/Yf57ZrwC6uZL25luJm3yyuXdvUk5Na3i3xBqGvaizXyNb+XkLbEEBB9D39TWLQAUUUUAFFFFABRRRQAUUV1ngDwYfEt4bi5BGnwMN//TRv7o/r/wDXoAZ4Q8BXfiZhcSE2ung8ykcv7KP69PrXrei+HdP0CAJZWwibGGkIy7fU/wCRWjDElvEI4lCRqAqqowAB2FTUAFFFFABRRRQBk614e0/X7cxXtskuB8r9HX6N1FeSeMPAF14bLXEJN1p+f9bj5o/Zh/X+Ve41DNElxEY5VDxsCrKwyCD2NAHzVRXXeP8AwX/wjl0Lq1UnTpmwB18pv7p9vT/OeRoAKKKKACiiigC1pWnTaxqNvZQDMszhRnoPUn2Ayfwr6B0fS4NG06CytxtiiXaD3J7k+5PNedfCDRhJcXepuuRH+4iPueW/TH5mvVaACiiigAooooAKKKKACiiigChq2mw6vYT2c4DQzKVPqPQj3B5r5+1fS5dG1O5s5v8AWQuVzj7w7H8Rg19IV5b8YNGAaz1RF+9+4lI/NT/6F+lAHm1FFFABRRRQB7r8PLAWHhKw4w0qmZj67jkfpiumqlpFuLbSrKHp5cKJ+SgVdoAKKKKACiiigAooooAKKKKACub8e2H9peE9QTHzRxmZT6FfmP6Aj8a6SquoxC5sbqI9HiZfzBFAHzdRRRQB/9k=";
      }
      return new Spacebars.SafeString('<img src="data:image/jpeg;base64,'+val+'">')
    }},
    { data: 'Nome',title:'Nome'},
    { data: 'Cognome',title:'Cognome'},
    { data: 'tespol', title: 'N tessera'},
    { data: 'Nazione', title: 'Nazionalità'},
    { data: 'codice',title:'Codice'},
    { data: 'birthday', title: 'Età',
      render: function(value){
        return moment().diff(value, 'years');
      }
    },
    { data: 'codice', title: 'Azioni',
      render: function (value) {
        return new Spacebars.SafeString('<a href="/profilopers/'+value+'" class="btn btn-primary">Profilo</a>&nbsp;<a href="/badgepdf/'+value+'" class="btn btn-primary" target="_blank">BADGE</a>')
      }
    }
  ]
});
