if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("ElencoPartiti", TabularTables);
TabularTables.ElencoPartiti = new Tabular.Table({
  name: "ElencoPartiti",
  collection: Persons,
  limit: 500,
  selector: function() {
    return {"status": "partito"}
  },
  order: [[3, "desc"]],
  columns: [
    { data: 'Nome', title: 'Nome'},
    { data: 'Cognome', title: 'Cognome'},
    { data: 'codice', title: 'Codice' },
    { data: 'dataPartenzaCamp', title: 'Partenza', sortDirection: 'descending', render:function(value){return moment(value).format('DD/MM/YYYY HH:mm');}},
    { data: 'motivoPartenzaCamp', title: 'Motivo'},
    { data: 'codice', title: 'Azioni', render:function(val){return new Spacebars.SafeString('<button class="btn btn-warning btnRecover" id="RE'+val+'">Recupera</button>');}}
  ]
});

Meteor.isClient && Template.registerHelper("ElencoPartenze", TabularTables);
TabularTables.ElencoPartenze = new Tabular.Table({
  name: "ElencoPartenze",
  collection: Persons,
  limit: 500,
  selector: function() {
    return {"status": "presente"}
  },
  order: [[2, "desc"]],
  columns: [
    { data: 'Nome', title: 'Nome'},
    { data: 'Cognome', title: 'Cognome'},
    { data: 'codice', title: 'Codice' },
    { data: 'birthday', title: 'Età',
      render: function(value){
        return moment().diff(value, 'years');
      }
    },
    { data: 'codice', title: 'Azioni', render:function(val){return new Spacebars.SafeString('<button class="btn btn-primary btnPartilo" id="RE'+val+'">Partenza</button>');}}
  ]
});
