if (!this.TabularTables) {
  this.TabularTables = {};
}

Meteor.isClient && Template.registerHelper("LibroEventi", TabularTables);
TabularTables.LibroEventi = new Tabular.Table({
  name: "LibroEventi",
  collection: Log,
  limit: 1000,
  order: [[0, "desc"]],
  columns: [
    { data: 'quando', title: 'Quando',sortOrder: 0, sortDirection: 'descending',render:function(val){ return moment(val).format("YYYY/MM/DD HH:mm");}},
    { data: 'mitt', title: 'Staff', render:function(val){
      var pers=Meteor.users.findOne({_id:val});
      return pers.profile.nome+" "+pers.profile.cognome;
    }},
    { data: 'dest', title: 'Ospite',render:function(value){
      var pers=Persons.findOne({"codice":value});
      if(pers){
        return pers.Nome+" "+pers.Cognome;
      }
    }},
    { data: 'dest', title: 'Codice Ospite' },
    { data: 'cosa', title: 'Azione'}
  ]
});
