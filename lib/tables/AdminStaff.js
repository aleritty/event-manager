if (!this.TabularTables) {
  this.TabularTables = {};
}
Meteor.isClient && Template.registerHelper("ElencoStaff", TabularTables);
TabularTables.ElencoStaff = new Tabular.Table({
  name: "ElencoStaff",
  collection: Meteor.users,
  limit: 500,
  order: [[1, "desc"],[0, "desc"]],
  columns: [
    {data: 'profile.nome', title:'Nome'},
    {data: 'profile.cognome', title:'Cognome'},
    { data: 'profile.telefono', title: 'Telefono'},
    { data: 'emails.0.address', title: 'Email'},
    { data: '_id', title: 'Azioni',render:function(val){
      return new Spacebars.SafeString('<a href="/admin-staff/'+val+'" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a><a href="/badgepdf/'+val+'" class="btn btn-sm btn-primary">Badge</a>');
    }},
  ]
});

Meteor.isClient && Template.registerHelper("LogFiltratoStaff", TabularTables);
TabularTables.LogFiltratoStaff = new Tabular.Table({
  name: "LogFiltratoStaff",
  collection: Log,
  limit: 500,
  order: [0, "desc"],
  columns: [
    { data: 'quando', title: 'Quando',sortOrder: 0, sortDirection: 'descending'},
    { data: 'dest', title: 'Ospite',render:function(value){
      var pers=Persons.find({codice:value},{limit: 1}).fetch();
      if(typeof pers == "object" && pers.length>0){
        return pers[0].Nome+" "+pers[0].Cognome;
      }
    }},
    { data: 'dest', title: 'Codice Ospite' },
    { data: 'cosa', title: 'Azione'}
  ]
});
