Template.reportpresenti.events({
    'click .export': function(event){
      $( event.target ).button( 'loading' );
    }
});

Template.reportpresenti.helpers({
  'adesso':function(){
    return moment().format("HH:mm [del] D/MM/YYYY");
  },
  'Persone':function(){
    report={'presenti':[],'assenti':[]};
    var ricerca=[];
    var inizio=new Date(2);
    var persFuori=Movimenti.find({"rientro":{$lt:inizio}},{fields:{"chi":1}});
    persFuori.forEach(function(uno){
      var pers=Persons.findOne({"status":"presente","codice":uno.chi},{fields:{"Nome":1,"Cognome":1,"codice":1}});
      // if(typeof pers!== "undefined"){
        ricerca.push(pers.codice);
        report["assenti"].push(pers);
      // }else{
      //   console.log("undefined");
      //   console.log(uno);
      // }
    });
    report['presenti']=Persons.find({"status":"presente",codice:{$nin:ricerca}},{fields:{"Nome":1,"Cognome":1,"codice":1}}).fetch();
    return report;
  },
});

var FilterDate = new Deps.Dependency;

Template.reportpasti.onCreated(function() {
  this.subscribe('LibroLog');
});

Template.reportpasti.onRendered(function() {
  $(document).ready(function(){
    $('.input-daterange').datepicker({
        format: "dd/mm/yyyy",
        maxViewMode: 1,
        todayBtn: "linked",
        language: "it",
        autoclose: true,
        todayHighlight: true
    });
    // $('#reportpasti').DataTable({
    //   paging: false,
    //   searching: false
    // });
  });
});


Template.reportpasti.helpers({
  TitAdesso:function(){
    return moment().format("HH:mm [del] DD/MM/YYYY");
  },
  oggi:function(){
    return moment().format("DD/MM/YYYY");
  },
  Pastiserv:function(){
    FilterDate.depend()
    var ValpickAfter=$('#pickAfter').val();
    var ValpickBefore=$('#pickBefore').val();
    if(moment(ValpickBefore,"DD/MM/YYYY").isValid()){
      var pickAfter=moment(ValpickAfter,"DD/MM/YYYY").toDate();pickAfter.setHours(0,0,0);
      var pickBefore=moment(ValpickBefore,"DD/MM/YYYY").toDate();pickBefore.setHours(23,59,59);
    }else{
      var pickAfter=new Date();pickAfter.setHours(0,0,0);
      var pickBefore=new Date();pickBefore.setHours(23,59,59);
    }
    var colazioni=Log.find({"cosa":'Colazione', "quando":{"$gt":pickAfter,"$lt":pickBefore}},{sort:{quando:1}}).fetch();
    var pranzi=Log.find({"cosa":'Pranzo', "quando":{"$gt":pickAfter,"$lt":pickBefore}},{sort:{quando:1}}).fetch();
    var cene=Log.find({"cosa":'Cena', "quando":{"$gt":pickAfter,"$lt":pickBefore}},{sort:{quando:1}}).fetch();

    var dictColazioni = _.countBy(colazioni,function(pasto){
      var appr=pasto.quando;appr.setHours(0,0,0);
      return appr
    });
    var dictPranzi = _.countBy(pranzi,function(pasto){
      var appr=pasto.quando;appr.setHours(0,0,0);
      return appr
    });
    var dictCene = _.countBy(cene,function(pasto){
      var appr=pasto.quando;appr.setHours(0,0,0);
      return appr
    });

    var pas=[];
    Object.keys(dictCene).forEach(function(key){
      var obj={"Giorno":key,"Colazioni":dictColazioni[key],"Pranzi":dictPranzi[key],"Cene":dictCene[key]};
      var tempo=new Date(obj.Giorno);
      var newgiorno=moment(tempo).format("YYYY/MM/DD");
      obj.Giorno=newgiorno;
      pas.push(obj);
    })
    return pas;
  }
});

Template.reportpasti.events({
  "change .inputdate": function(event, template){
     event.preventDefault();
     FilterDate.changed();
  }
});
