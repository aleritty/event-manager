AutoForm.hooks({
  insertStaffForm:{
    onSubmit: function(doc) {
      this.event.preventDefault();
      SchStaff.clean(doc);
      if(doc.Password==""){
        FlashMessages.sendError('La Password è obbligatoria');
      }else{
        Meteor.call('createStaff',doc.Nome,doc.foto,doc.associazione,doc.Cognome,doc.Email,doc.Telefono,doc.Password,doc.permessi);
        FlashMessages.sendSuccess('Staff Inserito');
        this.resetForm();
      }
      this.done();
      return false;
    }
  },
  updateStaffForm:{
    onSubmit: function(doc) {
      this.event.preventDefault();
      SchStaff.clean(doc);
      result=true;
      Meteor.call('modStaff',this.docId,doc.Nome,doc.foto,doc.associazione,doc.Cognome,doc.Email,doc.Telefono,doc.permessi,doc.Password);
      if(result){
        this.resetForm();
        FlashMessages.sendSuccess('Dati modificati');
      }else{
        FlashMessages.sendError('Errore');
      }
      this.done();
      return false;
    },
    onError:function(err){
      console.log("errore");
      console.log(err);
    }
  }
});

Template.AdminStaff.helpers({
  "persona": function(){
    var cod=this.codice;
    if(typeof cod != "undefined"){
      var opers=Meteor.users.findOne(cod);
      var pers={
        _id:cod,
        Nome:opers.profile.nome,
        Cognome:opers.profile.cognome,
        Telefono:opers.profile.telefono,
        Email:opers.emails[0].address,
        foto:opers.profile.foto,
        associazione:opers.profile.associazione,
        permessi:opers.roles
      };
      return pers;
    }else{
      return false;
    }
  },
  LogFiltrato: function(){
    return {"mitt":this.codice};
  }
});

Template.AdminStaff.events({
  'click .btnDel': function(event){
    event.preventDefault();
    var ret=confirm('Sei sicuro di volerlo eliminare?');
    if(ret){
      Meteor.call('delStaff',this.codice);
    }
  }
});
