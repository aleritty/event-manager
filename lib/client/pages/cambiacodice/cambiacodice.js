Template.cambiacodice.onCreated(function() {
  this.subscribe('TotalPersons');
});

Template.cambiacodice.events({
  "submit #cambiacodice": function(e){
    e.preventDefault();
    var oldcode=e.target.oldcode.value;
    var newcode=e.target.newcode.value;
    var pers=Persons.findOne({"codice":oldcode},{fields:{Nome:1,Cognome:1,codice:1}});
    var conf=confirm('Stai per cambiare il codice di '+pers.Nome+' '+pers.Cognome+"\nQuesta operazione è irreversibile, confermi?");
    if(conf){
      Meteor.call('cambiacodice',oldcode,newcode);
    }
    FlashMessages.sendSuccess('Codice cambiato');
    e.target.oldcode.value="";
    e.target.newcode.value="";
    e.target.oldcode.focus;
  }
});
