Template.badge.onCreated(function() {
  this.subscribe('OnePerson',this.data.uId);
  this.subscribe('FeedStaff');
});

Template.badge.helpers({
  ospite: function(){
    var uId=this.uId;
    var myData={}
    var pers=Persons.findOne({codice:uId});
    if(typeof pers !=="undefined"){
      myData={
        foto: pers.foto,
        nome: pers.Nome,
        cognome: pers.Cognome,
        codice: uId,
        tessera: pers.tespol,
        stato: "OSPITE"
      }
      if(typeof pers.letti !== "undefined"){
        myData.sistemazione={camera: pers.letti.camera,letto:pers.letti.letto}
      }
    }else{
      var pers=Meteor.users.findOne(uId);
      if(typeof pers !=="undefined"){
        myData={
          "foto": pers.profile.foto,
          "nome": pers.profile.nome,
          "cognome": pers.profile.cognome,
          "stato": "STAFF"
        }
      }
    }
    return myData;
  }
});



Template.badge.events({
  "click #stampami": function(event){
    event.preventDefault();
    window.print();
  }
});
