// Template.registerHelper("isReady", function (sub) {
//   if(sub) {
//     return FlowRouter.subsReady(sub);
//   } else {
//     return FlowRouter.subsReady();
//   }
// });

Template.news.onCreated(function() {
  this.subscribe('FeedNews');
});

Template.unanews.onCreated(function() {
  this.subscribe('FeedNews');
});

Template.news.helpers({
  'News': function(){
      return News.find({},{sort: {datains : -1}, limit: 10 });
  }
});
Template.unanews.helpers({
  'formattedDate': function(){
    return moment(this.datains).format("DD/MM/YYYY HH:mm");
  }
});

Template.unanews.events({
  'click .cancellabtn': function(event){
    if(event.target.id==""){
      event.target.id=$(event.target).parent().attr('id');
    }
    if(typeof event.target.id=="string" && event.target.id.length>5){
      Meteor.call('deleteNews',event.target.id.substr(1));
    }
  }
});

Template.aMainTemplate.helpers({
  Nome: function(){
    var loggedInUser = Meteor.user();
    if (!loggedInUser) {
      return "";
    }else{
      return Meteor.user().profile.nome;
    }
  }
});
