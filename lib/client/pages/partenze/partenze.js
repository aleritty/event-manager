Template.Partenze.events({
  'submit #FormPartenzaInsert': function(event){
    event.preventDefault();
    var cod=$('#insPartenza').val();
    var mot=$('#motivoPartenza').val();
    Meteor.call('insertPart',cod,mot);
    Meteor.call('Loggamelo', cod, "Partenza per "+mot);
    $('#insPartenza').val('');
    $('#motivoPartenza').val('');
    $('#insPartenza').focus();
  },
  'click .btnRecover':function(event){
    event.preventDefault();
    Meteor.call('recuperaPart',event.target.id.slice(2));
  },
  'click .btnPartilo':function(event){
    event.preventDefault();
    Meteor.call('insertPart',event.target.id.slice(2),"Partenza standard dopo 7 giorni");
  }
});


Template.Partenze.helpers({
  oggi: function(){
    var unasett=moment().subtract(7,'days')._d;
    return {"dataIngrCamp":{$lt:unasett}}
  },
  domani: function(){
    var unaott=moment().subtract(8,'days')._d;
    return {"dataIngrCamp":{$lt:unaott}}
  }
});
