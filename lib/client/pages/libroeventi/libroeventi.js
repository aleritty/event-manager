var FilterDate = new Deps.Dependency;

Template.LibroEventi.onCreated(function() {
  this.subscribe('TotalPersons');
  this.subscribe('FeedStaff');
});

Template.LibroEventi.onRendered(function() {
  $(document).ready(function(){
    $('.input-daterange').datepicker({
        format: "dd/mm/yyyy",
        maxViewMode: 1,
        todayBtn: "linked",
        language: "it",
        autoclose: true,
        todayHighlight: true
    });
  });
});

Template.LibroEventi.helpers({
  oggi:function(){
    return moment().format("DD/MM/YYYY");
  },
  helptab:function(){
    FilterDate.depend();
    var ValpickAfter=$('#pickAfter').val();
    var ValpickBefore=$('#pickBefore').val();
    if(moment(ValpickBefore,"DD/MM/YYYY").isValid()){
      var pickAfter=moment(ValpickAfter,"DD/MM/YYYY").toDate();pickAfter.setHours(0,0,0);
      var pickBefore=moment(ValpickBefore,"DD/MM/YYYY").toDate();pickBefore.setHours(23,59,59);
    }else{
      var pickAfter=new Date();pickAfter.setHours(0,0,0);
      var pickBefore=new Date();pickBefore.setHours(23,59,59);
    }
    return {"quando":{"$gt":pickAfter,"$lt":pickBefore}};
  }
});

Template.LibroEventi.events({
  "change .inputdate": function(event, template){
     event.preventDefault();
     FilterDate.changed();
  }
});
