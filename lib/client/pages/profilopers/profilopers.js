AutoForm.hooks({
updatePersonForm:{
  onSuccess: function(operation, result, template) {
    // var prevdoc=this.currentDoc;
    // var moddoc=this.updateDoc.$set;
    // delete prevdoc._id;
    // newdoc={};
    // for (var key in prevdoc) {
    //   if (prevdoc.hasOwnProperty(key)) {
    //     if(prevdoc[key]!==moddoc[key]){
    //       newdoc[key]=prevdoc[key];
    //     }
    //   }
    // }
    // jdoc=JSON.stringify(newdoc);
    // Meteor.call('Loggamelo', this.currentDoc.codice, "Modifica dati ospite a "+jdoc);
    Meteor.call('Loggamelo', this.currentDoc.codice, "Modifica dati ospite");
    FlashMessages.sendSuccess('Modifica eseguita correttamente');
  }
}
});

Template.profilopers.onCreated(function() {
  this.subscribe('OnePerson',this.data.codice);
  this.subscribe('FeedStaff');
});

Template.profilopers.helpers({
  'selecPerson': function(e,t){
    return Persons.findOne({codice:this.codice});
  },
  'formatteddate': function(){
    return moment(this.date).format("YYYY/MM/DD HH:mm");
  },
  LogFiltrato: function(){
    return {dest:this.codice};
  }
});

Template.profilopers.events({
  "click #deletePerson": function(event, template){
    event.preventDefault();
    var cod=this.codice;
    var ret=confirm("Sei sicuro?\nGuarda che non si può tornare indietro!!");
    if(ret===true){
      Meteor.call('cancellaPers',cod);
      Meteor.call('Loggamelo', cod, "Scheda persona Cancellata");
    }
  }
});
