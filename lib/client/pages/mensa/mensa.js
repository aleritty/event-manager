Template.registerHelper("Tipopasto", function(){
  switch(moment().format("H")) {
  case "8":
  case "9":
  case "10":
    tipopasto="Colazione";
    break;
  case "12":
  case "13":
  case "14":
  case "15":
    tipopasto="Pranzo";
    break;
  case "18":
  case "19":
  case "20":
  case "21":
  case "22": //debug
  case "23": //debug
      tipopasto="Cena";
      break;
  default:
      tipopasto=false;
  };
  return tipopasto;
});

Template.colonnaBlip.onCreated(function() {
  this.subscribe('PersoneAgibili');
  this.subscribe('LogPasti');
});

// GroundSazi = new Ground.Collection('GroundSazi');
// var oggi= new Date();oggi.setHours(0,0,1);
// GroundSazi.observeSource(Log.find({"cosa":tipopasto, "quando":{"$gte": oggi}}).count());

Template.colonnaSazi.helpers({
  // sazi:function(){
  //   var oggi= new Date();oggi.setHours(0,0,1);
  //   return {"cosa":tipopasto, "quando":{"$gte": oggi}};
  // },
  countsazi:function(){
    var oggi= new Date();oggi.setHours(0,0,1);
    return Log.find({"cosa":tipopasto, "quando":{"$gte": oggi}}).count();
    // return GroundSazi;
  }
  // "countsazi":function(){
  //   // var dt = $('.DataTables_Table_0').DataTable();
  //   // return dt.length;
  // }
});

Template.colonnaBlip.events({
  'keyup #blipcodice':function(event){
    var oggi= new Date();oggi.setHours(0,0,1);
    var cod=event.target.value;
    if(cod.length>9){
      var exist=Persons.findOne({"codice":cod,"status":"presente"});
      if(exist){
        var giammangiato=Log.find({"cosa":tipopasto, "quando": { $gt:oggi },"dest":cod}).fetch();
        if(giammangiato.length===0){
          Meteor.call('InsertPasto', cod, tipopasto);
          FlashMessages.sendSuccess('Pasto inserito', { hideDelay: 750,autoScroll:false });
        }else{
          FlashMessages.sendError('Gia mangiato', { hideDelay: 750,autoScroll:false });
          alert('GIA MANGIATO');
        }
      }else{
        FlashMessages.sendError('Codice errato o persona che ha lasciato il campo!', { hideDelay: 2000,autoScroll:false });
        alert('ATTENZIONE: Codice errato o persona che ha lasciato il campo');
      }
      event.target.value="";
    }
  },
  // 'click #scanbtn': function () {
  //   if (Meteor.isCordova) {
  //     cordova.plugins.barcodeScanner.scan(
  //       function (result) {
  //         alert("We got a barcode\n" +
  //           "Result: " + result.text + "\n" +
  //           "Format: " + result.format + "\n" +
  //           "Cancelled: " + result.cancelled);
  //       },
  //       function (error) {
  //         alert("Scanning failed: " + error);
  //       }
  //    );
  //   }
  // }
});
