AutoForm.hooks({
insertPersonForm:{
  onSuccess: function() {
    Meteor.call('registraPersona',this.insertDoc.codice);
    FlashMessages.sendSuccess('Ospite '+this.insertDoc.Nome+' '+this.insertDoc.Cognome+' Inserito correttamente, avanti il prossimo!<br><br><a href="/badgepdf/'+this.insertDoc.codice+'" class="btn btn-xl btn-danger stampapdf" id="'+this.insertDoc.codice+'" target="_blank">Stampa badge</a>&nbsp;&lt;--ATTENZIONE: Stampalo solo se l\'ospite ha assegnato il posto letto!!',{ autoHide: false });
    Session.set('webcamSnap', undefined);
    $('.webcam').show();
  }
}
});

AutoForm.addInputType('webcam',{
  template:'wcamera',
  valueOut:function(){
    return this.attr('src').replace(/^data\:image\/\w+\;base64\,/, '');
  }
});


Template.wcamera.onRendered(function() {
  Session.set('webcamSnap', undefined);
    Webcam.on( 'error', function(err) {
        console.log(err); // outputs error to console instead of window.alert
    });

    Webcam.set({
        width: 320,
        height: 240,
        dest_width: 320,
        dest_height: 240,
        crop_width: 140,
        crop_height: 180,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach( '.webcam' );
});

Template.wcamera.onDestroyed(function(){
  Webcam.reset('.webcam');
  Webcam.reset();
});

Template.wcamera.events({
    'click .snap': function (ev) {
      ev.preventDefault();
      Webcam.snap( function(image) {
        Session.set('webcamSnap', image);
        $('.webcam').hide();
      })
    },

    'click .resnap': function (ev) {
      ev.preventDefault();
      Session.set('webcamSnap', undefined);
      Webcam.attach( '.webcam' );
      $('.webcam').show();
    }
});

Template.wcamera.helpers({
    image: function () {
      return Session.get('webcamSnap');
    }
});
