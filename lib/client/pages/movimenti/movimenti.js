Template.Movimenti.onCreated(function() {
  this.subscribe('TotalPersons');
  this.subscribe('FeedStaff');
});

Template.Movimenti.helpers({
  'numpersone': function(){
    var inizio=new Date(2);
    return Movimenti.find({"rientro":{$lt:inizio}}).count();
  }
});

Template.Movimenti.events({
  'click #insertUscita': function(event){
    event.preventDefault();
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['porta'])) {throw new Meteor.Error(403, "Access denied");}
    var cod=$('#blipcodice').val();
    var exists=Persons.findOne({codice:cod});
    if(exists.status=="presente"){
      Meteor.call('insertMov',cod,"Uscita");
    }else if(exists.status=="partito"){
      alert('ATTENZIONE: Questo ospite ha lasciato il campo! Non può effettuare movimenti!');
    }else{
      alert('Ospite non trovato, riprovare');
    }
    $('#blipcodice').val('');
    $('#blipcodice').focus();
  },
  'click #insertRientro': function(event){
    event.preventDefault();
    var loggedInUser = Meteor.user();if (!loggedInUser ||!Roles.userIsInRole(loggedInUser,['porta'])) {throw new Meteor.Error(403, "Access denied");}
    var cod=$('#blipcodice').val();
    var exists=Persons.findOne({codice:cod});
    if(exists.status=="presente"){
      Meteor.call('insertMov',cod,"Rientro");
    }else if(exists.status=="partito"){
      alert('ATTENZIONE: Questo ospite ha lasciato il campo! Non può effettuare movimenti!');
    }else{
      alert('Ospite non trovato, riprovare');
    }
    $('#blipcodice').val('');
    $('#blipcodice').focus();
  },
  'click #exportCSV': function(event){
    $( event.target ).button( 'loading' );
  }
});
