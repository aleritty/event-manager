Log = new Mongo.Collection('log');
Log.allow({
  insert: function(userId, doc) {
    return !! userId;
  },
  update: function(userId, doc) {
    return !! userId;
  },
  remove: function() {
    return false;
  }
});
Schema={};
Log.attachSchema(new SimpleSchema({
      dest: {
          type: String
      },
      mitt: {
          type: String
      },
      quando: {
          type: Date,
          label: "Data azione",
          defaultValue: moment().toISOString(),
          autoform: {
            value: moment().toISOString()
          }
      },
      cosa: {
          type: String
      }
}));
Ground.Collection(Log);
