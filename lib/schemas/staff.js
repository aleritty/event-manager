SchStaff=new SimpleSchema({
  Nome: {
    type: String
  },
  Cognome: {
    type: String
  },
  Telefono: {
    type: String,
    autoform: {
      type: 'tel'
    }
  },
  Email: {
    type: String,
    autoform: {
      type: 'email'
    }
  },
  foto:{
    type: String,
    optional:true,
    autoform:{
      type: 'webcam'
    }
  },
  associazione: {
    type: String,
    optional:true
  },
  Password: {
    type: String,
    min: 6,
    max: 50,
    autoform: {
      type: 'password'
    },
    optional:true
  },
  ConfPassword: {
    type: String,
    min: 6,
    max: 50,
    autoform: {
      type: 'password',
      label: 'Conferma Password'
    },
    optional:true,
    custom: function(){
      if(this.value!="" && this.field('Password').value!=""){
        if (this.value !== this.field('Password').value) {
            return 'noMatch';
        }
      }
    }
  },
  permessi: {
    type: [String],
    label: "Permessi",
    minCount:1,
    autoform:{
      type:"select-checkbox",
      options:{
        "porta":"Registrare i movimenti",
        "letti":"gestire camere e letti",
        "mensa":"Servire i pasti",
        "registrazione":"Registrare nuovi utenti",
        "regstaff":"Registrare nuovi staff",
        "modpers":"modificare i dati di un utente",
        "modstaff":"Modificare i dati di uno staff",
        "delpers":"Cancella utente",
        "delstaff":"cancella staff",
        "newsint":"Scrivere news interne",
        // "newsext":"Scrivere news esterne",
        "toteventi":"vedere gli eventi di tutti"
      }
    }
  }
});

SchStaff.messages({
  noMatch: "Le password non coincidono",
});
