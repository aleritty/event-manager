News = new Mongo.Collection('news');
News.allow({
  insert: function(userId, doc) {
    return !! userId;
  },
  update: function(userId, doc) {
    return !! userId;
  },
  remove: function(userId, doc) {
    return !! userId;
  }
});
News.attachSchema(new SimpleSchema({
      authId: {
          type: String,
          autoValue: function() {
              return Meteor.user()._id;
          },
          autoform: {
            type: "hidden"
          }
      },
      autore: {
        type: String,
        autoValue: function() {
          return Meteor.user().profile.nome+" "+Meteor.user().profile.cognome;
        },
        autoform: {
          type: "hidden"
        }
      },
      titolo: {
          type: String
      },
      datains: {
          type: Date,
          label: "Data inserimento news",
          defaultValue:moment().toISOString(),
          autoform: {
            type: "hidden",
            value: moment().toISOString()
          }
      },
      testo: {
          type: String,
          autoform: {
            rows: 10,
            type: 'summernote',
            //settings: // summernote options goes here
          }
      }
}));
