Persons = new Mongo.Collection('persons');
Persons.allow({
  insert: function(userId, doc) {
    return !! userId;
  },
  update: function(userId, doc) {
    return !! userId;
  },
  remove: function(doc) {
    return !! Meteor.userId();
  }
});
Schema.Persons=new SimpleSchema({
  Nome: {
      type: String
  },
  Cognome: {
      type: String
  },
  Nazione:{
    type: String,
    label: "Nazionalità"
  },
  Telefono: {
    type: String,
    optional: true,
    autoform: {
      type: "tel"
    }
  },
  tespol: {
    type: String,
    optional: true,
    label: "N° Tessera",
    unique: true
  },
  birthday: {
      type: Date,
      label: "Data di Nascita",
      autoform: {
        type: "bootstrap-datepicker",
        datePickerOptions:{
          format: "dd/mm/yyyy",
          startView: 1,
          todayBtn: "linked",
          language: "it",
          autoclose: true
        }
      }
  },
  luognasc: {
      type: String,
      label: "Luogo di nascita"
  },
  vismedsel: {
      type: String,
      allowedValues: ["SI","NO"],
      label: "Visita medica effettuata"
  },
  vismeddate: {
      type: Date,
      label: "Data Visita Medica",
      optional:true,
      autoform: {
        type: "bootstrap-datepicker",
        datePickerOptions:{
          format: "dd/mm/yyyy",
          todayBtn: "linked",
          language: "it",
          autoclose: true
        }
      }
  },
  permsogsel: {
      type: String,
      allowedValues: ["SI","NO"],
      label: "Permesso di soggiorno"
  },
  permsogdate: {
      type: Date,
      label: "Data scadenza permesso",
      optional:true,
      autoform: {
        type: "bootstrap-datepicker",
        datePickerOptions:{
          format: "dd/mm/yyyy",
          startView: 1,
          todayBtn: "linked",
          language: "it",
          autoclose: true
        }
      }
  },
  richassel: {
      type: String,
      allowedValues: ["SI","NO"],
      label: "Richiesta asilo politico"
  },
  richasdate: {
      type: Date,
      label: "Data richiesta asilo",
      optional:true,
      autoform: {
        type: "bootstrap-datepicker",
        datePickerOptions:{
          format: "dd/mm/yyyy",
          startView: 1,
          todayBtn: "linked",
          language: "it",
          autoclose: true
        }
      }
  },
  ingrdate: {
      type: Date,
      label: "Data ingresso italia",
      optional:true,
      autoform: {
        type: "bootstrap-datepicker",
        datePickerOptions:{
          format: "dd/mm/yyyy",
          startView: 1,
          todayBtn: "linked",
          language: "it",
          autoclose: true
        }
      }
  },
  codfisc:{
    type: String,
    optional: true,
    label: "Codice fiscale",
    min: 16,
    max: 16,
    unique: true
  },
  codice:{
    type: String,
    autoValue: function() {
      if(this.isInsert && !this.isSet){
        return(new Date().getTime() / 1000).toFixed(0);
      }else{
        return this.value;
      }
    },
    unique: true,
    autoform:{
      type: 'hidden'
    }
  },
  createdAt: {
    type: Date,
    defaultValue: moment().toISOString(),
    denyUpdate: true,
    autoform: {
      type: 'hidden',
      value: moment().toISOString()
    }
  },
  status: {
    type: String,
    allowedValues: ['registrato','presente','partito','cancellato'],
    autoform: {
      type: 'hidden',
      defaultValue: 'registrato'
    }
  },
  dataIngrCamp: {
    type: Date,
    label: "Data di ingresso al campo",
    optional: true,
    autoform: {
      type: "bootstrap-datepicker",
      datePickerOptions:{
        format: "dd/mm/yyyy",
        startView: 1,
        todayBtn: "linked",
        language: "it",
        autoclose: true
      }
    }
  },
  dataPartenzaCamp: {
    type: Date,
    label: "Data di partenza dal campo",
    optional: true,
    autoform:{
      type:'hidden'
    }
  },
  motivoPartenzaCamp: {
    type: String,
    optional: true,
    label: "Motivo di partenza dal campo",
    autoform:{
      type: 'hidden'
    }
  },
  sesso: {
    type: String,
    allowedValues: ['M', 'F']
  },
  letti: {
    type: Object,
    optional: true,
    label: "Sistemazione"
  },
  "letti.camera": {
    type: Number,
    optional: true
  },
  "letti.letto": {
    optional: true,
    type: Number
  },
  "history": {
    type: [String],
    optional: true,
    label: "Storico presenze al campo"
  },
  "presenza":{
    type: String,
    allowedValues: ['interno','esterno'],
    autoform:{
      type:'hidden',
      defaultValue: 'interno'
    }
  },
  "foto":{
    type: String,
    autoform:{
      type: 'webcam'
    }
  },
  "note":{
    type: String,
    optional: true,
    autoform: {
      rows: 6
    }
  }
});
Persons.attachSchema(Schema.Persons);
