Movimenti = new Mongo.Collection('movimenti');
Movimenti.allow({
  insert: function(userId, doc) {
    return !! userId;
  },
  update: function(userId, doc) {
    return !! userId;
  },
  remove: function() {
    return false;
  }
});
Movimenti.attachSchema(new SimpleSchema({
  uscita: {
    type: Date,
    label: "Data uscita"
  },
  rientro: {
    type: Date,
    label: "Data rientro"
  },
  chi: {
    type: String,
    label: "Codice persona"
  },
  staff:{
    type: String,
    label: "Codice staff"
  }
}));
