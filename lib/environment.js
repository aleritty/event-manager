
//// Impediamo la creazione di account se non si è admin
AccountsTemplates.configure({
    forbidClientAccountCreation: true
});

/// Impostiamo le opzioni di internazionalizzazione ///////////////
i18n.setLanguage('it');
moment.locale('it');
T9n.setLanguage('it');

SimpleSchema.messages({
  required: "[label] è obbligatorio",
  minString: "[label] must be at least [min] characters",
  maxString: "[label] non può essere più di [max] caratteri",
  minNumber: "[label] deve essere almeno [min]",
  maxNumber: "[label] deve essere al massimo [max]",
  minDate: "[label] deve essere almeno il [min]",
  maxDate: "[label] non può essere dopo [max]",
  badDate: "[label] non è una data",
  notAllowed: "[value] non è un valore ammissibile",
  expectedString: "[label] deve essere un testo",
  expectedNumber: "[label] deve essere un numero",
  expectedConstructor: "[label] deve essere un [type]",
  regEx: [
    {msg: "[label] failed regular expression validation"},
    {exp: SimpleSchema.RegEx.Email, msg: "[label] deve essere un indirizzo email valido"},
    {exp: SimpleSchema.RegEx.WeakEmail, msg: "[label] deve essere un indirizzo email valido"},
  ],
  keyNotInSchema: "[key] non è prevista in questo modulo"
});

if (Meteor.isClient) {
    $.extend(true, $.fn.dataTable.defaults, {
        language: {
          "decimal":        "",
          "emptyTable":     "Non ho trovato niente",
          "info":           "Mostro da _START_ a _END_ di _TOTAL_ risultati",
          "infoEmpty":      "Mostro da 0 a 0 di 0 risultati",
          "infoFiltered":   "(filtrato da _MAX_ risultati totali)",
          "infoPostFix":    "",
          "thousands":      ",",
          "lengthMenu":     "Mostra _MENU_ risultati",
          "loadingRecords": "Caricamento...",
          "processing":     "Elaborazione...",
          "search":         "Cerca:",
          "zeroRecords":    "Nessun risultato corrisponde",
          "paginate": {
              "first":      "Primo",
              "last":       "Ultimo",
              "next":       "Prossimo",
              "previous":   "Precedente"
          },
          "aria": {
              "sortAscending":  ": clicca per ordinare ascendente",
              "sortDescending": ": clicca per ordinare discendente"
          }
      }
    });
    Meteor.Spinner.options = {
        lines: 13,
        length: 10,
        width: 5,
        radius: 15, // The radius of the inner circle
        corners: 0.7,
        rotate: 0,
        direction: 1,
        color: '#fff',
        speed: 0.7,
        trail: 60,
        shadow: true,
        hwaccel: false,
        className: 'spinner',
        zIndex: 2e9,
        top: 'auto',
        left: 'auto'
    };
}


//// Assicuriamoci che in startup tutto sia ok
Meteor.startup(function () {
  if(Meteor.isServer){
    /////// Indice per impedire di mettere due persone nello stesso letto //////
    Persons._ensureIndex({"letti.letto":1,"letti.camera":1},{"unique": true,"sparse": true,"name":"uniciletti"});
    ///funzione base in caso di nuova installazione
    if(Meteor.users.find().count()<1){
      console.log("Non ci sono utenti, creazione utente amministratore di base")
      Meteor.call('PrimoStaff');
    }
  }
});


/////////// Sicurezza per impedire il backup del DB a chi non è autorizzato ////
if (Meteor.isServer){
  appDump.allow = function(){
    return !! this.user._id;
  }
}
